# carsharing parking

To generate the launchable program run:
```sh
mvn package appassembler:assemble
```
Then it can be found and launched with:
```sh
sh target/appassembler/bin/carsharing
```
To see available commandline arguments use `--help`:
```sh
sh target/appassembler/bin/carsharing --help
```


