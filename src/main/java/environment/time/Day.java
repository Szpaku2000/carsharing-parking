package environment.time;
/**
 * Followed this: https://stackoverflow.com/a/17664546
 * to improve the enum introducing an iterator like
 * functionality.
 */
public enum Day {

    MONDAY("Monday"),
    TUESDAY("Tuesday"),
    WEDNESDAY("Wednesday"),
    THURSDAY("Thursday"),
    FRIDAY("Friday"),
    SATURDAY("Saturday"),
    SUNDAY("Sunday"){
        @Override
        public Day next() {
            return MONDAY; // see below for options for this line
        }
    };

    public Day next() {
        // No bounds checking required here, because the last instance overrides
        return values()[ordinal() + 1];
    }
    public Day previous(){
        return this.next().next().next().next().next().next(); //lol
    }

    private final String name;

    Day(String name) {
        this.name = name;
    }

    public String toString(){
        return this.name;
    }
}