package cars.tasks;
public interface Task{
    public boolean isFinished();
    public int getTimeSpent();
    public int getDuration();
}